# My Wiki


Ceci est un Wiki personnel, regroupant toutes les connaissances que j'apprends au fur et à mesure du temps.


### Comment utiliser ce Wiki ?

+ Clonez ce projet

` git clone https://gitlab.com/MrChoppy/mywiki.git `

+ Lancez  [Zim](https://zim-wiki.org/)

+ Cliquez sur fichier > ouvrir un autre bloc notes > ajouter

+ Ajoutez le repo cloné